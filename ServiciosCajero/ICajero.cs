﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ServiciosCajero
{
    [DataContract]
    public class Cliente
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Telefono { get; set; }
        [DataMember]
        public string Mail { get; set; }
    }

    public class OrdenVenta
    {
        [DataMember]
        public int DocEntry { get; set; }
        [DataMember]
        public int DocNum { get; set; }
        [DataMember]
        public string CardCode { get; set; }
        [DataMember]
        public string CardName { get; set; }
        [DataMember]
        public String DocDate { get; set; }
        [DataMember]
        public string Moneda { get; set; }
        [DataMember]
        public double TotalMXN { get; set; }
        [DataMember]
        public double TotalExtranjero { get; set; }
        [DataMember]
        public double TotalImpuestosMXN { get; set; }
        [DataMember]
        public double TotalImpuestosExtranjero { get; set; }
        [DataMember]
        public double PagadoMXN { get; set; }
        [DataMember]
        public double PagadoExtranjero { get; set; }
        [DataMember]
        public string ItemCode { get; set; }
        [DataMember]
        public string ItemName { get; set; }
        [DataMember]
        public double Quantity { get; set; }
        [DataMember]
        public double PrecioUnitario { get; set; }
        [DataMember]
        public double PorcentajeDescuento { get; set; }
        [DataMember]
        public double TotalLineaSinImpuestos { get; set; }
        [DataMember]
        public string CodigoImpuesto { get; set; }
        [DataMember]
        public double SumaImpuestosLineaMXN { get; set; }
        [DataMember]
        public double SumaImpuestosLineaExtranjero { get; set; }


    }

    public class depositoempleado
    {
        [DataMember]
        public string NOM { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public double Cantidad { get; set; }


    }


    public class cupon
    {
        [DataMember]
        public int DocEntry { get; set; }
        [DataMember]
        public string CardCode { get; set; }
        [DataMember]
        public double Cantidad { get; set; }
        [DataMember]
        public String Comentarios { get; set; }


    }

    public class user
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string NOM { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string Huella { get; set; }
        [DataMember]
        public int Admin { get; set; }
    }

    public class userupdate
    {

        public string NOM { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string Huella { get; set; }
        [DataMember]
        public string Solution { get; set; }
    }


    public class PagoOrdenes
    {
        [DataMember]
        public int DocEntry { get; set; }
        [DataMember]
        public double Cantidad { get; set; }

    }

    public class solution
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string UId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Latitude { get; set; }
        [DataMember]
        public string Longitude { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public int User { get; set; }
    }

    public class money_transaction
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Users { get; set; }
        [DataMember]
        public string Details { get; set; }
        [DataMember]
        public int Solution { get; set; }
        [DataMember]
        public IList<document> Documents { get; set; }
    }

    public class device_event
    {

        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string Event { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public int Solution { get; set; }
    }

    public class document
    {

        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public double Count { get; set; }
        [DataMember]
        public string Currency { get; set; }
        [DataMember]
        public string Endpoint { get; set; }
    }

    public class accounting
    {

        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public double Count { get; set; }
        [DataMember]
        public string Currency { get; set; }
        [DataMember]
        public string CurrentPoint { get; set; }

    }
    public class QR
    {
        [DataMember]  
        public string token { get; set; }
    }
    public class Respuesta
    {
        [DataMember]
        public string success { get; set; }
    }
    public class Estado
    {
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string Value { get; set; }
    }


    [ServiceContract]
    public interface ICajero
    {
        
        [OperationContract]
        [WebGet(UriTemplate = "/getOrdenVenta/{DocEntry}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<OrdenVenta> getOrdenVenta(string DocEntry);

        [OperationContract]
        [WebGet(UriTemplate = "/getDepositoEmpleado/{NOM}/{Solucion}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<depositoempleado> getDepositoEmpleado(string NOM,string Solucion);

        [OperationContract]
        [WebGet(UriTemplate = "/getCupon/{DocEntry},{CardCode}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<cupon> getCupon(string DocEntry, string CardCode);

        [OperationContract]
        [WebGet(UriTemplate = "/getUsers/{Solucion}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<user> getUsers(string Solucion);

        [OperationContract]
        [WebInvoke(UriTemplate = "/PagoOrdenVenta", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, Method = "POST")]
        PagoOrdenes PagoOrdenVenta(PagoOrdenes Orden);

        [OperationContract]
        [WebInvoke(UriTemplate = "/createuser", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, Method = "POST")]
        userupdate createuser(userupdate Usuario);

        [OperationContract]
        [WebInvoke(UriTemplate = "/createsolution", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, Method = "POST")]
        solution createsolution(solution Solucion);

        [OperationContract]
        [WebInvoke(UriTemplate = "/createmoney_transaction", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, Method = "POST")]
        money_transaction createmoney_transaction(money_transaction transaccion);

        // /UpdateChange/{token}
        //la que lo actualiza solo necesita el token del cupon.
        // regresa un success: ok, err
        [OperationContract]
        [WebInvoke(UriTemplate = "/updateChangeState/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, Method = "POST")]
        Respuesta updateChangeState(QR t);
        // la que revisa solo necesita el token del cupon. //
        //regresa un state: on:dinero a dar?, off, non exitent
        [OperationContract]
        [WebGet(UriTemplate = "/getChangeState/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Estado getChangeState(string token);

        //[OperationContract]
        //[WebInvoke(UriTemplate = "/createdocument", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, Method = "POST")]
        //document createdocument(document Documento);

        //[OperationContract]
        //[WebInvoke(UriTemplate = "/createaccounting", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, Method = "POST")]
        //accounting createaccounting(accounting Accounting);

        [OperationContract]
        [WebInvoke(UriTemplate = "/createdevice_event", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, Method = "POST")]
        device_event createdevice_event(device_event Evento);
        //user InsertaUsuario(string Id, string UserName, string Password, string Admin);
        //string InsertaUsuario(string jsonObject);

        //[OperationContract]
        //[WebInvoke(UriTemplate = "/Obtenciondecatalogodeempleados", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //void Delete(int idCliente);
        //[OperationContract]
        //[WebInvoke(UriTemplate = "/Registrodepalmadeempleado", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST")]
        //void Delete(int idCliente);
        //[OperationContract]
        //[WebInvoke(UriTemplate = "/Registrodealertas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST")]
        //void Delete(int idCliente);
        //[OperationContract]
        //[WebInvoke(UriTemplate = "/Registrodedotacion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST")]
        //void Delete(int idCliente);
        //[OperationContract]
        //[WebInvoke(UriTemplate = "/Registroderetiro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST")]
        //void Delete(int idCliente);
        //[OperationContract]
        //[WebInvoke(UriTemplate = "/Registrodepagodeservicios", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST")]
        //void Delete(int idCliente);

        //[OperationContract]
        //[WebInvoke(UriTemplate = "/Registrodepagodeordendeventa", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST")]
        //void Delete(int idCliente);
        //[OperationContract]
        //[WebInvoke(UriTemplate = "/Consultademontodenomina", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //void Delete(int idCliente);
        //[OperationContract]
        //[WebInvoke(UriTemplate = "/Registroderetirodenomina", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST")]
        //void Delete(int idCliente);

        [OperationContract]
        [WebGet(UriTemplate = "/getAll",ResponseFormat = WebMessageFormat.Json,BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<Cliente> GetAll();

        [OperationContract]
        [WebGet(UriTemplate = "/get/{idCliente}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Cliente Get(string idCliente);

        [OperationContract]
        [WebInvoke(UriTemplate = "/create", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, Method ="POST")]
        Cliente Create(Cliente cliente);

        [OperationContract]
        [WebInvoke(UriTemplate = "/update", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped)]
        Cliente Update(Cliente cliente);

        [OperationContract]
        [WebInvoke(UriTemplate = "/delete", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST")]
        void Delete(int idCliente);


    }
}
