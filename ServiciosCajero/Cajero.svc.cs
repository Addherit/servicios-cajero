﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;



namespace ServiciosCajero
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Cajero" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Cajero.svc o Cajero.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Cajero : ICajero
    {
        private List<Cliente> repo;
        private List<OrdenVenta> repo2;
       


        public Cajero()
        {
            repo = new List<Cliente>();
            repo.Add(new Cliente() { Id = 1, Mail = "mail1@google.com", Nombre = "Cliente1", Telefono = "1111" });
            repo.Add(new Cliente() { Id = 2, Mail = "mail2@google.com", Nombre = "Cliente2", Telefono = "2222" });
            repo.Add(new Cliente() { Id = 3, Mail = "mail3@google.com", Nombre = "Cliente3", Telefono = "3333" });
            repo.Add(new Cliente() { Id = 4, Mail = "mail4@google.com", Nombre = "Cliente4", Telefono = "4444" });
            repo2 = new List<OrdenVenta>();
            
        }
       
        public List<OrdenVenta> getOrdenVenta(string DocEntry)
        {
            int id = 0;
            int.TryParse(DocEntry, out id);
            string sql ="SELECT T0.DocEntry, T0.DocNum, T0.CardCode, T2.CardName, T0.DocDate, T0.DocCur Moneda, T0.DocTotal TotalMXN, T0.DocTotalFC TotalExtranjero,T0.VatSum TotalImpuestosMXN, T0.VatSumFC TotalImpuestosExtranjero, T0.PaidToDate PagadoMXN, T0.PaidFC PagadoExtranjero, T1.ItemCode, T3.ItemName, T1.Quantity, T1.Price PrecioUnitario, T1.DiscPrcnt PorcentajeDescuento, T1.LineTotal TotalLineaSinImpuestos,T1.TaxCode CodigoImpuesto, T1.VatSum SumaImpuestosLineaMXN, T1.VatSumFrgn SumaImpuestosLineaExtranjero FROM ORDR T0 INNER JOIN RDR1 T1 ON T1.DocEntry = T0.DocEntry INNER JOIN OCRD T2 ON T2.CardCode = T0.CardCode LEFT JOIN OITM T3 ON T3.ItemCode = T1.ItemCode where T0.DocNum = " + id + " and T0.DocNum not in (Select DocEntry from  cajero.dbo.pagos_ordenes) and T0.DocStatus = 'O'";
            //docentry sql comentado
            //string sql = "SELECT T0.DocEntry, T0.DocNum, T0.CardCode, T2.CardName, T0.DocDate, T0.DocCur Moneda, T0.DocTotal TotalMXN, T0.DocTotalFC TotalExtranjero, T0.VatSum TotalImpuestosMXN, T0.VatSumFC TotalImpuestosExtranjero, T0.PaidToDate PagadoMXN, T0.PaidFC PagadoExtranjero, T1.ItemCode, T3.ItemName, T1.Quantity, T1.Price PrecioUnitario, T1.DiscPrcnt PorcentajeDescuento, T1.LineTotal TotalLineaSinImpuestos , T1.TaxCode CodigoImpuesto, T1.VatSum SumaImpuestosLineaMXN, T1.VatSumFrgn SumaImpuestosLineaExtranjero FROM ORDR T0 INNER JOIN RDR1 T1 ON T1.DocEntry = T0.DocEntry INNER JOIN OCRD T2 ON T2.CardCode = T0.CardCode LEFT JOIN OITM T3 ON T3.ItemCode = T1.ItemCode where T0.DocEntry = " + id + " and T0.[DocEntry] not in (Select DocEntry from  cajero.dbo.pagos_ordenes) and T0.DocStatus = 'O'";
            //string sql = "SELECT ID, Clave as Cliente, Descripcion as Estatus,0 as Importe, 0 as IVA, 0 as Total   FROM Cadena";
            SqlDataAdapter da = new SqlDataAdapter(sql, ConfigurationManager.ConnectionStrings["Connection"].ToString());
            DataSet ds = new DataSet();
            da.Fill(ds);

            DataTable dt = ds.Tables[0];

            var convertedList = (from rw in dt.AsEnumerable()
                                 select new OrdenVenta()
                                 {
                                     DocEntry = Convert.ToInt32(rw["DocEntry"]),
                                     DocNum = Convert.ToInt32(rw["DocNum"]),
                                     CardCode = Convert.ToString(rw["CardCode"]),
                                     CardName = Convert.ToString(rw["CardName"]),
                                     DocDate = Convert.ToString(rw["DocDate"]),
                                     Moneda = Convert.ToString(rw["Moneda"]),
                                     TotalMXN = Convert.ToDouble(rw["TotalMXN"]),
                                     TotalExtranjero = Convert.ToDouble(rw["TotalExtranjero"]),
                                     TotalImpuestosMXN = Convert.ToDouble(rw["TotalImpuestosMXN"]),
                                     TotalImpuestosExtranjero = Convert.ToDouble(rw["TotalImpuestosExtranjero"]),
                                     PagadoMXN = Convert.ToDouble(rw["PagadoMXN"]),
                                     PagadoExtranjero = Convert.ToDouble(rw["PagadoExtranjero"]),
                                     ItemCode = Convert.ToString(rw["ItemCode"]),
                                     ItemName = Convert.ToString(rw["ItemName"]),
                                     Quantity = Convert.ToDouble(rw["Quantity"]),
                                     PrecioUnitario = Convert.ToDouble(rw["PrecioUnitario"]),
                                     PorcentajeDescuento = Convert.ToDouble(rw["PorcentajeDescuento"]),
                                     TotalLineaSinImpuestos = Convert.ToDouble(rw["TotalLineaSinImpuestos"]),
                                     CodigoImpuesto = Convert.ToString(rw["CodigoImpuesto"]),
                                     SumaImpuestosLineaMXN = Convert.ToDouble(rw["SumaImpuestosLineaMXN"]),
                                     SumaImpuestosLineaExtranjero = Convert.ToDouble(rw["SumaImpuestosLineaExtranjero"])
                                 }).ToList();

            return convertedList;
           
           
        }
        public List<depositoempleado> getDepositoEmpleado(string NOM,string Solucion)
        {
            
            string sql = "SELECT  d.[NOM],u.[Nombre], SUM(d.[Cantidad]) as Cantidad FROM[Cajero].[dbo].[depositos_empleados] d  inner join [user] u on u.[NOM] = d.[NOM] where d.[NOM] = '" + NOM + "' and d.retirado =  0 and u.Status = 'ALTA'  and u.Solution ='"+Solucion+ "'  and d.Solution ='" + Solucion + "' group by d.[NOM], u.[Nombre]";
            //string sql = "SELECT ID, Clave as Cliente, Descripcion as Estatus,0 as Importe, 0 as IVA, 0 as Total   FROM Cadena";
            SqlDataAdapter da = new SqlDataAdapter(sql, ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
            DataSet ds = new DataSet();
            da.Fill(ds);

            DataTable dt = ds.Tables[0];

            var convertedList = (from rw in dt.AsEnumerable()
                                 select new depositoempleado()
                                 {
                                    
                                     NOM = Convert.ToString(rw["NOM"]),
                                     Nombre = Convert.ToString(rw["Nombre"]),
                                     Cantidad = Convert.ToDouble(rw["Cantidad"])


                                 }).ToList();

            return convertedList;


        }

        public List<cupon> getCupon(string DocEntry, string CardCode)
        {

            string sql = "SELECT  [DocNum] as DocEntry,[CardCode], [DocTotal] as Cantidad , [Comentarios] = 'Prueba' FROM ORIN  where[DocNum] = '" + DocEntry + "' and [CardCode] = '"+CardCode+"' and [DocEntry] not in (Select DocEntryNC from  cajero.dbo.pagos_notascredito) and [DocStatus] = 'O'";
            //string sql = "SELECT ID, Clave as Cliente, Descripcion as Estatus,0 as Importe, 0 as IVA, 0 as Total   FROM Cadena";
            SqlDataAdapter da = new SqlDataAdapter(sql, ConfigurationManager.ConnectionStrings["Connection"].ToString());
            DataSet ds = new DataSet();
            da.Fill(ds);

            DataTable dt = ds.Tables[0];

            var convertedList = (from rw in dt.AsEnumerable()
                                 select new cupon()
                                 {

                                     DocEntry = Convert.ToInt32(rw["DocEntry"]),
                                     CardCode = Convert.ToString(rw["CardCode"]),
                                     Cantidad = Convert.ToDouble(rw["Cantidad"]),
                                     Comentarios = Convert.ToString(rw["Comentarios"])

                                 }).ToList();

            return convertedList;


        }
        public void getReturnMoney(string DocEntry, string CardCode)//servicio del retorno 
        {
            //el query por modificar para la busqueda del token de detalles
            string sql = "SELECT  [DocNum] as DocEntry,[CardCode], [DocTotal] as Cantidad ," +
                " [Comentarios] = 'Prueba' FROM ORIN  where[DocNum] = '" + DocEntry + "' and [CardCode] = '" + CardCode + "' and [DocEntry] " +
                "not in (Select DocEntryNC from  cajero.dbo.pagos_notascredito) and [DocStatus] = 'O'";
            //string sql = "SELECT ID, Clave as Cliente, Descripcion as Estatus,0 as Importe, 0 as IVA, 0 as Total   FROM Cadena";
            SqlDataAdapter da = new SqlDataAdapter(sql, ConfigurationManager.ConnectionStrings["Connection"].ToString());
            DataSet ds = new DataSet();
            da.Fill(ds);

            DataTable dt = ds.Tables[0];

            var convertedList = (from rw in dt.AsEnumerable()
                                 select new cupon()
                                 {

                                     DocEntry = Convert.ToInt32(rw["DocEntry"]),
                                     CardCode = Convert.ToString(rw["CardCode"]),
                                     Cantidad = Convert.ToDouble(rw["Cantidad"]),
                                     Comentarios = Convert.ToString(rw["Comentarios"])

                                 }).ToList();

           // return convertedList;
        }


        public List<user> getUsers(string solu)
        {

            string sql = "SELECT  [Id],[NOM], [Nombre], [Password], [Huella], [Admin] from [User] where Status = 'ALTA'and solution =" + solu;
            //string sql = "SELECT ID, Clave as Cliente, Descripcion as Estatus,0 as Importe, 0 as IVA, 0 as Total   FROM Cadena";
            SqlDataAdapter da = new SqlDataAdapter(sql, ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
            DataSet ds = new DataSet();
            da.Fill(ds);

            DataTable dt = ds.Tables[0];

            var convertedList = (from rw in dt.AsEnumerable()
                                 select new user()
                                 {
                                     Id = Convert.ToInt32(rw["Id"]),
                                     NOM = Convert.ToString(rw["NOM"]),
                                     Nombre = Convert.ToString(rw["Nombre"]),
                                     Password = Convert.ToString(rw["Password"]),
                                     Huella = Convert.ToString(rw["Huella"]),
                                     Admin = Convert.ToInt32(rw["Admin"])

                                 }).ToList();

            return convertedList;


        }
        public PagoOrdenes PagoOrdenVenta(PagoOrdenes OrdenVenta)
        {
            if (OrdenVenta != null)
            {
                try
                {
                    //string sql = "Insert into user (Id, UserName, Password, Admin, CreateAt) values ("+IdUser+",'"+UserName+"','"+Password+"', "+Admin+", getdate()" ;

                    var cmd = new SqlCommand("insert into [pagos_ordenes] (DocEntry, Cantidad, CreatedAt, StatusSap) values (" + OrdenVenta.DocEntry + ", '" + OrdenVenta.Cantidad + "', getdate(), 0)");
                    SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                    cmd.Connection = conexion;
                    conexion.Open();
                    cmd.ExecuteNonQuery();
                    conexion.Close();

                }
                catch (InvalidCastException e)
                {

                }
            }
            return OrdenVenta;
        }
        public userupdate createuser(userupdate Usuario)
        {
            if (Usuario != null)
            {
                try
                {
                    //string sql = "Insert into user (Id, UserName, Password, Admin, CreateAt) values ("+IdUser+",'"+UserName+"','"+Password+"', "+Admin+", getdate()" ;
                    var cmd = new SqlCommand("Update [user] set Password = '"+ Usuario.Password + "', Huella = '" + Usuario.Huella + "' where NOM =  '" + Usuario.NOM + "' and Solution ='"+Usuario.Solution+"'");
                    //var cmd = new SqlCommand("insert into [user] (Id, UserName, PassWord, Admin, CreatedAt) values (" + Usuario.Id + ", '" + Usuario.UserName + "', '" + Usuario.Password + "', " + Usuario.Admin + ", getdate())");
                    SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                    cmd.Connection = conexion;
                    conexion.Open();
                    cmd.ExecuteNonQuery();
                    conexion.Close();

                }
                catch (InvalidCastException e)
                {
                    
                }
            }
            return Usuario;
        }

       public solution createsolution(solution Solucion)
        {
            if (Solucion != null)
            {
                try
                {
                    //string sql = "Insert into user (Id, UserName, Password, Admin, CreateAt) values ("+IdUser+",'"+UserName+"','"+Password+"', "+Admin+", getdate()" ;

                    var cmd = new SqlCommand("insert into solution (Id, UId, Name, Address, Latitude, Longitude, Status, [User], createdat) values (" + Solucion.Id + ", '" + Solucion.UId + "', '" + Solucion.Name + "', '" + Solucion.Address + "', '" + Solucion.Latitude + "', '" + Solucion.Longitude + "', '" + Solucion.Status + "', " + Solucion.User + ", getdate())");
                    SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                    cmd.Connection = conexion;
                    conexion.Open();
                    cmd.ExecuteNonQuery();
                    conexion.Close();

                    
                }
                catch (InvalidCastException e)
                {

                }
            }
            return Solucion;
        }

       
        public money_transaction createmoney_transaction(money_transaction Transaccion)
        {
            if (Transaccion != null)
            {
                try
                {
                    //string sql = "Insert into user (Id, UserName, Password, Admin, CreateAt) values ("+IdUser+",'"+UserName+"','"+Password+"', "+Admin+", getdate()" ;

                    var cmd = new SqlCommand("insert into money_transaction (Id, Type, Users, Details, Solution, CreatedAt) values (" + Transaccion.Id + ", '" + Transaccion.Type + "', '" + Transaccion.Users + "', '" + Transaccion.Details + "', " + Transaccion.Solution + ", getdate());SELECT SCOPE_IDENTITY()");
                    SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                    cmd.Connection = conexion;
                    conexion.Open();
                    int IdMoneyTransaction = Convert.ToInt32(cmd.ExecuteScalar());
                    conexion.Close();
                    int solu = Convert.ToInt32(Transaccion.Solution);
                    if (Transaccion.Type == "withdrawal")
                    {

                      



                        //Seleccionar los billetes y monedas en cashbox y poner transacciones de retiro = 0
                        //Query para obtener lo que tiene el cajero
                        //SELECT t.Type, t.Value, t.Count, t.Currency, t.CurrentPoint, t.[Transaction]  FROM accounting t  WHERE t.[Transaction] = (SELECT MAX( [Transaction] )  FROM accounting)                          
                        //Seleccionar los billetes y monedas en cashbox y dispenser y poner transacciones de retiro = 0
                        string sql = "SELECT distinct t.Type, t.Value, t.Currency, t.CurrentPoint  FROM accounting t inner join money_transaction t2 on t.IdMoneyTransaction = t2.IdMoneyTransaction WHERE  t.CurrentPoint = 'cashbox' and t2.Solution = " + Transaccion.Solution + "";
                        //string sql = "SELECT ID, Clave as Cliente, Descripcion as Estatus,0 as Importe, 0 as IVA, 0 as Total   FROM Cadena";
                        SqlDataAdapter da = new SqlDataAdapter(sql, ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        DataTable dt = ds.Tables[0];


                        foreach (DataRow row in dt.Rows)
                        {
                            
                                double valor = Convert.ToDouble(row[1]);
                            if (valor >= 1)
                            {
                                var cmd2 = new SqlCommand("insert into accounting (Type, Value, Count, Currency, CurrentPoint, [Transaction], IdMoneyTransaction) values ('" + row[0] + "', " + valor + ", 0, '" + row[2] + "',  'cashbox', " + Transaccion.Id + ", " + IdMoneyTransaction + ")");
                                SqlConnection conexion2 = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                                cmd2.Connection = conexion2;
                                conexion2.Open();
                                cmd2.ExecuteNonQuery();
                                conexion2.Close();
                            }
                                else
                            {
                                var cmd2 = new SqlCommand("insert into accounting (Type, Value, Count, Currency, CurrentPoint, [Transaction], IdMoneyTransaction) values ('" + row[0] + "', 0.5, 0, '" + row[2] + "',  'cashbox', " + Transaccion.Id + ", " + IdMoneyTransaction + ")");
                                SqlConnection conexion2 = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                                cmd2.Connection = conexion2;
                                conexion2.Open();
                                cmd2.ExecuteNonQuery();
                                conexion2.Close();
                            }
                        }

                    }
                    else if (Transaccion.Type == "full_withdrawal")
                    {
                        //Query para obtener lo que tiene el cajero
                        //SELECT t.Type, t.Value, t.Count, t.Currency, t.CurrentPoint, t.[Transaction]  FROM accounting t  WHERE t.[Transaction] = (SELECT MAX( [Transaction] )  FROM accounting)                          
                        //Seleccionar los billetes y monedas en cashbox y dispenser y poner transacciones de retiro = 0
                        string sql = " SELECT distinct t.Type, t.Value, t.Currency, t.CurrentPoint  FROM accounting t inner join money_transaction t2 on t.IdMoneyTransaction = t2.IdMoneyTransaction WHERE  t.CurrentPoint = 'dispenser' and t2.Solution = " + Transaccion.Solution + "";
                        //string sql = "SELECT ID, Clave as Cliente, Descripcion as Estatus,0 as Importe, 0 as IVA, 0 as Total   FROM Cadena";
                        SqlDataAdapter da = new SqlDataAdapter(sql, ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        DataTable dt = ds.Tables[0];


                        foreach (DataRow row in dt.Rows)
                        {
                            double valor = Convert.ToDouble(row[1]);

                            if (valor >= 1)
                            {
                                var cmd2 = new SqlCommand("insert into accounting (Type, Value, Count, Currency, CurrentPoint, [Transaction], IdMoneyTransaction) values ('" + row[0] + "', " + valor + ", 0, '" + row[2] + "',  'dispenser', " + Transaccion.Id + ", " + IdMoneyTransaction + ")");
                                SqlConnection conexion2 = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                                cmd2.Connection = conexion2;
                                conexion2.Open();
                                cmd2.ExecuteNonQuery();
                                conexion2.Close();
                            }
                               else
                            {
                                var cmd2 = new SqlCommand("insert into accounting (Type, Value, Count, Currency, CurrentPoint, [Transaction], IdMoneyTransaction) values ('" + row[0] + "', 0.5, 0, '" + row[2] + "',  'dispenser', " + Transaccion.Id + ", " + IdMoneyTransaction + ")");
                                SqlConnection conexion2 = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                                cmd2.Connection = conexion2;
                                conexion2.Open();
                                cmd2.ExecuteNonQuery();
                                conexion2.Close();
                            }
                        }
                        string sql2 = "SELECT distinct t.Type, t.Value, t.Currency, t.CurrentPoint  FROM accounting t inner join money_transaction t2 on t.IdMoneyTransaction = t2.IdMoneyTransaction WHERE  t.CurrentPoint = 'cashbox' and t2.Solution = " + Transaccion.Solution + "";
                        //string sql = "SELECT ID, Clave as Cliente, Descripcion as Estatus,0 as Importe, 0 as IVA, 0 as Total   FROM Cadena";
                        SqlDataAdapter da2 = new SqlDataAdapter(sql2, ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                        DataSet ds2 = new DataSet();
                        da2.Fill(ds2);
                        DataTable dt2 = ds2.Tables[0];


                        foreach (DataRow row in dt2.Rows)
                        {
                            double valor = Convert.ToDouble(row[1]);
                            if (valor >= 1)
                            {

                            
                                var cmd2 = new SqlCommand("insert into accounting (Type, Value, Count, Currency, CurrentPoint, [Transaction], IdMoneyTransaction) values ('" + row[0] + "', " + valor + ", 0, '" + row[2] + "',  'cashbox', " + Transaccion.Id + ", " + IdMoneyTransaction + ")");
                            SqlConnection conexion2 = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                            cmd2.Connection = conexion2;
                            conexion2.Open();
                            cmd2.ExecuteNonQuery();
                            conexion2.Close();
                            }
                            else
                            {
                                var cmd2 = new SqlCommand("insert into accounting (Type, Value, Count, Currency, CurrentPoint, [Transaction], IdMoneyTransaction) values ('" + row[0] + "', 0.5, 0, '" + row[2] + "',  'cashbox', " + Transaccion.Id + ", " + IdMoneyTransaction + ")");
                                SqlConnection conexion2 = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                                cmd2.Connection = conexion2;
                                conexion2.Open();
                                cmd2.ExecuteNonQuery();
                                conexion2.Close();
                            }
                        }
                    }
                    else
                    { 

                    foreach (var obj in Transaccion.Documents)
                    {
                        createdocument(obj, Transaccion.Id, IdMoneyTransaction,solu);

                       
                        //if (Transaccion.Type != "full_withdrawal" && Transaccion.Type != "withdrawal")
                        //{
                            if (obj.Endpoint == "out")
                            {
                                string sql = "SELECT top 1 Count from accounting a inner join money_transaction m on a.IdMoneyTransaction = m.IdMoneyTransaction and m.Solution = " + Transaccion.Solution + " where a.Type = '" + obj.Type + "' and a.Value = " + obj.Value + "  and a.Currency = '" + obj.Currency + "'  and a.CurrentPoint = 'dispenser' and m.Solution = "+Transaccion.Solution+" order by a.[Transaction] desc";
                                //string sql = "SELECT ID, Clave as Cliente, Descripcion as Estatus,0 as Importe, 0 as IVA, 0 as Total   FROM Cadena";
                                SqlDataAdapter da = new SqlDataAdapter(sql, ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                                DataSet ds = new DataSet();
                                da.Fill(ds);

                                DataTable dt = ds.Tables[0];
                                
                                if (dt.Rows.Count == 0)
                                {
                                }
                               
                                else
                                {
                                    double cantidad = Convert.ToDouble(dt.Rows[0][0]);
                                    cantidad = cantidad - obj.Count;
                                    if (cantidad == 0)
                                    {

                                    }
                                    else
                                    {
                
                                    var cmd2 = new SqlCommand("insert into accounting (Type, Value, Count, Currency, CurrentPoint, [Transaction], IdMoneyTransaction) values ('" + obj.Type + "', " + obj.Value + ", " + cantidad + ", '" + obj.Currency + "',  'dispenser', " + Transaccion.Id + ", "+ IdMoneyTransaction + ")");
                                    SqlConnection conexion2 = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                                    cmd2.Connection = conexion2;
                                    conexion2.Open();
                                    cmd2.ExecuteNonQuery();
                                    conexion2.Close();
                                    }
                                }
                            }
                            else
                            {
                                string sql = "SELECT top 1 Count from accounting a inner join money_transaction m on a.IdMoneyTransaction = m.IdMoneyTransaction where a.Type = '" + obj.Type + "' and a.Value = " + obj.Value + "  and a.Currency = '" + obj.Currency + "'  and a.CurrentPoint = '" + obj.Endpoint + "' and m.Solution = "+Transaccion.Solution+" order by a.[Transaction] desc";
                                //string sql = "SELECT ID, Clave as Cliente, Descripcion as Estatus,0 as Importe, 0 as IVA, 0 as Total   FROM Cadena";
                                SqlDataAdapter da = new SqlDataAdapter(sql, ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                                DataSet ds = new DataSet();
                                da.Fill(ds);
                                DataTable dt = ds.Tables[0];
                               
                                
                                if (dt.Rows.Count == 0)
                                {
                                    var cmd2 = new SqlCommand("insert into accounting (Type, Value, Count, Currency, CurrentPoint, [Transaction], IdMoneyTransaction) values ('" + obj.Type + "', " + obj.Value + ", " + obj.Count + ", '" + obj.Currency + "',  '" + obj.Endpoint + "', " + Transaccion.Id + ", "+ IdMoneyTransaction + ")");
                                    SqlConnection conexion2 = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                                    cmd2.Connection = conexion2;
                                    conexion2.Open();
                                    cmd2.ExecuteNonQuery();
                                    conexion2.Close();
                                }
                                else
                                {
                                    double cantidad = Convert.ToDouble(dt.Rows[0][0]);
                                    cantidad = cantidad + obj.Count;
                                    var cmd2 = new SqlCommand("insert into accounting (Type, Value, Count, Currency, CurrentPoint, [Transaction], IdMoneyTransaction) values ('" + obj.Type + "', " + obj.Value + ", " + cantidad + ", '" + obj.Currency + "',  '" + obj.Endpoint + "', " + Transaccion.Id + ", "+ IdMoneyTransaction + ")");
                                    SqlConnection conexion2 = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                                    cmd2.Connection = conexion2;
                                    conexion2.Open();
                                    cmd2.ExecuteNonQuery();
                                    conexion2.Close();
                                }
                            }
                        }

                    }

                    //}    
                       
                        
                  

                }
                catch (InvalidCastException e)
                {

                }
               
            }
            return Transaccion;
        }

        public document createdocument(document Documento, int IdTransaccion, int IdMoneyTransaction,int IdSolucion)
        {
            if (Documento != null)
            {
                try
                {
                    
                    double valor = 0;
                    double.TryParse(Documento.Value, out valor);
                    if (IdSolucion == 1)
                    {
                        if (valor == 5)
                        {
                            var cmd = new SqlCommand("insert into document ( Type, Value, Count, Currency, Endpoint, [Transaction], IdMoneyTransaction) values ('" + Documento.Type + "', 0.5, " + Documento.Count + ", '" + Documento.Currency + "',  '" + Documento.Endpoint + "', " + IdTransaccion + ", " + IdMoneyTransaction + ")");
                            SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                            cmd.Connection = conexion;
                            conexion.Open();
                            cmd.ExecuteNonQuery();
                            conexion.Close();

                        }
                        else
                        {
                            var cmd = new SqlCommand("insert into document ( Type, Value, Count, Currency, Endpoint, [Transaction], IdMoneyTransaction) values ('" + Documento.Type + "', " + Documento.Value + ", " + Documento.Count + ", '" + Documento.Currency + "',  '" + Documento.Endpoint + "', " + IdTransaccion + ", " + IdMoneyTransaction + ")");
                            SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                            cmd.Connection = conexion;
                            conexion.Open();
                            cmd.ExecuteNonQuery();
                            conexion.Close();
                        }
                    }else
                    {
                        var cmd = new SqlCommand("insert into document ( Type, Value, Count, Currency, Endpoint, [Transaction], IdMoneyTransaction) values ('" + Documento.Type + "', " + Documento.Value + ", " + Documento.Count + ", '" + Documento.Currency + "',  '" + Documento.Endpoint + "', " + IdTransaccion + ", " + IdMoneyTransaction + ")");
                        SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                        cmd.Connection = conexion;
                        conexion.Open();
                        cmd.ExecuteNonQuery();
                        conexion.Close();
                    }

                    //string sql = "Insert into user (Id, UserName, Password, Admin, CreateAt) values ("+IdUser+",'"+UserName+"','"+Password+"', "+Admin+", getdate()" ;




                }
                catch (InvalidCastException e)
                {

                }
            }
            return Documento;
        }

        public accounting createaccounting(accounting Accounting, int IdTransaccion)
        {
            if (Accounting != null)
            {
                try
                {
                    double valor = 0;
                    double.TryParse(Accounting.Value, out valor);
                    //string sql = "Insert into user (Id, UserName, Password, Admin, CreateAt) values ("+IdUser+",'"+UserName+"','"+Password+"', "+Admin+", getdate()" ;

                    var cmd = new SqlCommand("insert into accounting (Type, Value, Count, Currency, CurrentPoint, [Transaction]) values ('" + Accounting.Type + "', " + valor + ", " + Accounting.Count + ", '" + Accounting.Currency + "',  '" + Accounting.CurrentPoint + "', " + IdTransaccion + ")");
                    SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                    cmd.Connection = conexion;
                    conexion.Open();
                    cmd.ExecuteNonQuery();
                    conexion.Close();

                }
                catch (InvalidCastException e)
                {

                }
            }
            return Accounting;
        }

        public device_event createdevice_event(device_event Evento)
        {
            if (Evento != null)
            {
                try
                {
                    //string sql = "Insert into user (Id, UserName, Password, Admin, CreateAt) values ("+IdUser+",'"+UserName+"','"+Password+"', "+Admin+", getdate()" ;

                    var cmd = new SqlCommand("insert into device_event ( Status, Event, Message, Solution, Createdat) values ( '" + Evento.Status + "', '" + Evento.Event + "', '" + Evento.Message + "', " + Evento.Solution + ",  getdate())");
                    SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                    cmd.Connection = conexion;
                    conexion.Open();
                    cmd.ExecuteNonQuery();
                    conexion.Close();

                }
                catch (InvalidCastException e)
                {

                }
            }
            return Evento;
        }

      
        public Respuesta updateChangeState(QR t)
        {
            Respuesta r;
            r = new Respuesta();
            r.success = "err";
            if (t.token != null)
            {
                try
                    {
                        var cmd = new SqlCommand("update transaction_change set status ='inactivo' where token ='"+t.token+"'");
                        SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                        cmd.Connection = conexion;
                        conexion.Open();
                        cmd.ExecuteNonQuery();
                        conexion.Close();
                        r.success = "ok";
                        return r;
                    }
                    catch (SqlException e)
                    {
                        return r;
                    }
            }
            return r;
        }
        public Estado getChangeState(string token) // Esta funcion optione el token del qr y checa si es valido
        {
            Estado r = new Estado();
            r.Status = "err";
            if (token != null) {
                try
                {
                    string sql = "select status, Value from transaction_change where Token ='" + token + "'";
                    SqlDataAdapter da = new SqlDataAdapter(sql, ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    DataTable dt = ds.Tables[0];
                    string est = dt.Rows[0][0].ToString();
                    string val = dt.Rows[0][1].ToString();

                    if (est == "activo")
                    {
                        r.Status = "valid";
                        r.Value = val;
                        return r;
                    }
                    else if (est == "inactivo")
                    {
                        r.Status = "invalid";
                        r.Value = val;
                        return r;
                    }
                }
                catch (SqlException e)
                {
                    return r;
                }
               
            }
            return r;   
        }



        //public string InsertaUsuario(string jsonObject)
        //{
        //    //int id = 0;
        //    //int.TryParse(IdUser, out id);
        //    try
        //    {
        //        //string sql = "Insert into user (Id, UserName, Password, Admin, CreateAt) values ("+IdUser+",'"+UserName+"','"+Password+"', "+Admin+", getdate()" ;

        //        var cmd = new SqlCommand("insert into user (Id, UserName, PassWord, Admin) select* from OPENJSON(" + jsonObject + ") WITH(Id Int, UserName nvarchar(50), Password nvarchar(50), Admin Int)");
        //        SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
        //        cmd.Connection = conexion;
        //        cmd.ExecuteNonQuery();
        //        //string sql = "SELECT ID, Clave as Cliente, Descripcion as Estatus,0 as Importe, 0 as IVA, 0 as Total   FROM Cadena";
        //        //    SqlDataAdapter da = new SqlDataAdapter(sql, ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
        //        //DataSet ds = new DataSet();
        //        //da.Update(ds);
        //        return "Sincronizado correctamente";
        //    }
        //    catch (InvalidCastException e)
        //    {
        //        return "La sincronización del usuario fallo";
        //    }
        //}
        //public user InsertaUsuario(string Id, string UserName, string Password, string Admin)
        //{
        //    int IdUser = 0;
        //    int.TryParse(Id, out IdUser);
        //    int iAdmin = 0;
        //    int.TryParse(Admin, out iAdmin);

        //        //string sql = "Insert into user (Id, UserName, Password, Admin, CreateAt) values ("+IdUser+",'"+UserName+"','"+Password+"', "+Admin+", getdate()" ;

        //        var cmd = new SqlCommand("insert into user (Id, UserName, PassWord, Admin, CreatedAt) values ("+IdUser+", '"+UserName+"', '"+Password+"', "+iAdmin+", getdate()");
        //        SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
        //        cmd.Connection = conexion;
        //    conexion.Open();
        //        cmd.ExecuteNonQuery();
        //    conexion.Close();
        //        user usuario = new user();
        //            usuario.Id = IdUser;
        //        usuario.UserName = UserName;
        //        usuario.Password = Password;
        //        usuario.Admin = iAdmin;
        //        return usuario;
        //        //string sql = "SELECT ID, Clave as Cliente, Descripcion as Estatus,0 as Importe, 0 as IVA, 0 as Total   FROM Cadena";
        //        //    SqlDataAdapter da = new SqlDataAdapter(sql, ConfigurationManager.ConnectionStrings["ConnectionCajero"].ToString());
        //        //DataSet ds = new DataSet();
        //        //da.Update(ds);



        //}
        public Cliente Create(Cliente cliente)
        {
            if (cliente != null)
            {
                if (cliente.Id == 0)
                {
                    int maxCliente = repo.Max(c => c.Id);
                    cliente.Id = maxCliente + 1;
                }
                repo.Add(cliente);
            }
            return cliente;
        }

        public void Delete(int idCliente)
        {
            if (idCliente > 0)
            {
                Cliente cli = ObtenerCliente(idCliente);
                if (cli != null)
                {
                    repo.Remove(cli);
                }
            }
        }

        public Cliente Get(string idCliente)
        {
            int id = 0;
            int.TryParse(idCliente, out id);
            return ObtenerCliente(id);
        }

        private Cliente ObtenerCliente(int idCliente)
        {
            Cliente cli = repo.Where(c => c.Id == idCliente).Select(c => c).SingleOrDefault();
            return cli;
        }

        public List<Cliente> GetAll()
        {
            return repo;
        }

        public Cliente Update(Cliente cliente)
        {
            Cliente cli = null;
            if (cliente !=null)
            {
                cli = ObtenerCliente(cliente.Id);
                if (cli != null)
                {
                    cli.Nombre = cliente.Nombre;
                    cli.Mail= cliente.Mail;
                    cli.Telefono = cliente.Telefono;
                }
            }
            return cli;
        }
    }
}
